(function($) {
    var DELIMITER = ',';
    var FIRST_SAMPLE_IND = 19;
    var Events = {
        FILE_INPUT_CHANGE: 'change.bs.file.input',
        ALL_CHUNKS_READY: 'all.chunks.ready',
        RAW_LINES_READY: 'raw.lines.ready',
        ACTIVE_SAMPLE_CHANGE: 'active.sample.change',
        FILE_READING_START: 'file.reading.start',
        TABLE_INIT_START: 'table.init.start',
        TABLE_INIT_FINISH: 'table.init.finish'
    };
    var DISPLAYED_COLUMNS = ['Gene', 'Chromosome', 'Position', 'rsID', 'Ref', 'Alt', 'EffType', 'IVS', 'GT', 'DEPTH', 'SCORE', 'More'];
    var rawLines = [];
    var currentSamples = [];
    var db_head = [];
    var keyToIndex = {};
    var MIN_IVS = 3;

    var Renderer = {

        rsID: function(value) {
            if (value === '-') return value;

            var _rsId = value.substring(2);
            return $('<a>')
                .attr({
                    href: ' http://www.ncbi.nlm.nih.gov/SNP/snp_ref.cgi?type=rs&rs=' + _rsId,
                    target: '_blank'
                })
                .text(value)
                .prop('outerHTML');
        },

        gene: function(value) {
            return $('<a>').attr({
                href: "http://www.genecards.org/cgi-bin/carddisp.pl?gene=" + value,
                target: '_blank'
            }).text(value).prop('outerHTML');
        },

        /**
         * @return {string}
         */
        OMIM: function (value) {
            if (value === '-') return 'No OMIM record.';
            return 'OMIM record: ' + $('<a>').attr({ href: value, target: '_blank' }).text(value).prop('outerHTML');
        },

        clinVar: function(value) {
            if (value === '-') return 'No ClinVar record found.';

            var split = value.split(';');

            return $('<a>').attr({ href: split[2], target: '_blank' }).text('ClinVar').prop('outerHTML')
                + ': ' + split[0]
                + ', ' + split[1];
        },

        /**
         * @return {string}
         */
        HGVSName: function(value) {
            if (value === '-') return value;
            var names = $.map(value.split('/'), function(e) {
                return $('<code>').text(e).prop('outerHTML');
            });
            return names.join(', ');
        },

        nonZeroScorefoundIn: function(line_data, samples, current_sample) {
            var return_value = [current_sample];
            console.log(samples, current_sample, line_data);
            return_value = return_value.concat(
                samples.filter(function(sample) {
                    console.log(sample, keyToIndex[sample + '_SCORE'], line_data[keyToIndex[sample + '_SCORE']]);
                    return current_sample !== sample && line_data[keyToIndex[sample + '_SCORE']] > 0;
                }));
            return return_value.join(', ');
        }
    };

    function round1(d) {
        return Math.round(d * 10) / 10;
    }

    $(document).ready(function() {

        $('.open-popup-link').magnificPopup({
            type:'inline',
            midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
        });

        var $samples = $('#samples');
        var TABLE = $('#main-table').DataTable({

                "columnDefs": [
                    {
                        "targets": [11],
                        "searchable": false
                    },
                    {
                        "targets": [0, 3, 4, 5, 6, 8, 9, 11],
                        "sortable": false
                    },
                    {
                        "render": function ( data, type, row ) {
                            return Renderer.rsID(data);
                        },
                        "targets": 3  // rsID column
                    },
                    {
                        "render": function ( data, type, row ) {
                            return $('<a>').attr({
                                href: "#",
                                'data-sample': data.sample,
                                'data-line-ind': data.lineInd
                            }).addClass('info-popup').text('show').prop('outerHTML');
                        },
                        "targets": 11  // MORE column
                    },
                    {
                        "render": function ( data, type, row ) {
                            return Renderer.gene(data);
                        },
                        "targets": 0  // Gene column
                    },
                    {
                        "render": function ( data, type, row ) {
                            return data.length <= 5 ? data : data.length + ' bp';
                        },
                        "targets": [4, 5]  // Ref, Alt columns
                    }
                ],

            "order": [[10, 'desc']]
        });

        $(document).on(Events.RAW_LINES_READY, function(e) {
            e.preventDefault();

            currentSamples = $.map(db_head.slice(FIRST_SAMPLE_IND), function(value) {
                return value.split('_')[0];
            });
            currentSamples = $.unique(currentSamples);

            $samples.empty();
            $.each(currentSamples, function(i, v) {
                $samples.append($('<option>').text(v));
            });
            $samples.trigger('change');
        });

        $(document).on([Events.RAW_LINES_READY, Events.TABLE_INIT_FINISH].join(' '), function(e) {
            $('.spinner').hide();
        });

        $(document).on([Events.FILE_READING_START, Events.TABLE_INIT_START].join(' '), function(e) {
            $('.spinner').show();
        });

        $samples.on('change', function(e) {
            e.preventDefault();
            $(document).trigger(Events.ACTIVE_SAMPLE_CHANGE, [$samples.find('option:selected').text()]);
        });

        $(document).on(Events.ACTIVE_SAMPLE_CHANGE, function(e, value) {
            initTableContent(value);
        });

        $(document).on('click', 'a.info-popup', function(e) {
            e.preventDefault();

            var sample = $(e.target).data('sample');
            var lineInd = $(e.target).data('line-ind');
            var lineData = rawLines[parseInt(lineInd)];

            var $popup = $('#my-popup').clone();

            $popup.find('.data-Gene').html(Renderer.gene(lineData[keyToIndex['Gene']]));
            $popup.find('.data-Position').text(lineData[keyToIndex['Position']]);
            $popup.find('.data-EffType').text(lineData[keyToIndex['EffType']]);
            $popup.find('.data-IVS').text(lineData[keyToIndex['IVS']]);

            $popup.find('.data-GT').text(lineData[keyToIndex[sample + '_GT']]);
            $popup.find('.data-DEPTH').text(lineData[keyToIndex[sample + '_DEPTH']]);
            $popup.find('.data-SCORE').text(lineData[keyToIndex[sample + '_SCORE']]);


            $popup.find('.data-HGVS').html(Renderer.HGVSName(lineData[keyToIndex['HGVS_name']]));
            $popup.find('.data-rsID').html(Renderer.rsID(lineData[keyToIndex['rsID']]));

            $popup.find('.data-PROVEAN').text(lineData[keyToIndex['PROVEAN']]);
            $popup.find('.data-SIFT').text(lineData[keyToIndex['SIFT']]);
            $popup.find('.data-Polyphen2').text(lineData[keyToIndex['Polyphen2b']]);
            $popup.find('.data-fathmm-MKL').text(lineData[keyToIndex['fathmm-MKL']]);

            $popup.find('.data-1000G_AF').text(lineData[keyToIndex['1000G_AF']]);
            $popup.find('.data-ExAC_AF').text(lineData[keyToIndex['ExAC_AF']]);
            $popup.find('.data-ESP6500_AF').text(lineData[keyToIndex['ESP6500_AF']]);

            $popup.find('.data-our_AF').text(lineData[keyToIndex['our_AF']]);

            $popup.find('.data-OMIM').html(Renderer.OMIM(lineData[keyToIndex['OMIM']]));
            $popup.find('.data-ClinVar').html(Renderer.clinVar(lineData[keyToIndex['ClinVar']]));

            $popup.find('.data-found-in').text(Renderer.nonZeroScorefoundIn(lineData, currentSamples, sample));

            showPopupImage($popup);
        });

        function showPopupImage(src) {
            $.magnificPopup.open({
                items: {
                    src: $('<div>').addClass("white-popup").append(src),
                    type: 'inline'
                }
            })
        }

        $('#fileinput').on(Events.FILE_INPUT_CHANGE, parseFile);

        function initTableContent(sample) {
            console.log('init table');
            $(document).trigger(Events.TABLE_INIT_START);
            TABLE.clear().draw();

            $.each(rawLines.sort(function(a, b) { return b[keyToIndex[sample + '_SCORE']] - a[keyToIndex[sample + '_SCORE']]; }), function(lineInd, line) {
                if (lineInd % 10000 === 0) console.log(lineInd);

                if (parseFloat(line[keyToIndex[sample + '_SCORE']]) === 0) return;

                var values = $.map(DISPLAYED_COLUMNS, function(col) {
                    if (col == 'GT') {
                        return line[keyToIndex[sample + '_' + col]];
                    } else if (col == 'DEPTH') {
                        return line[keyToIndex[sample + '_DEPTH']];
                    } else if (col == 'SCORE') {
                        return line[keyToIndex[sample + '_SCORE']];
                    } else if (col == 'More') {
                        return {sample: sample, lineInd: lineInd};
                    } else {
                        return line[keyToIndex[col]];
                    }
                });
                TABLE.row.add(values);
            });
            console.log('init done');
            TABLE.draw();
            $(document).trigger(Events.TABLE_INIT_FINISH);
        }

        function parseLineOrNull(raw_line) {
            if (raw_line === "") return null;

            var elements = raw_line.split(DELIMITER);

            elements[keyToIndex['IVS']] = round1(parseFloat(elements[keyToIndex['IVS']]));
            if (elements[keyToIndex['IVS']] <= MIN_IVS) return null;

            elements[keyToIndex['rsID']] = elements[keyToIndex['rsID']].split(':')[0];

            for (var i = FIRST_SAMPLE_IND; i + 2 < elements.length; i += 3) {
                elements[i + 2] = round1(parseFloat(elements[i + 2]));
            }

            return elements;
        }

        function transformChunks(chunks) {
            var currTail = "";
            $.each(chunks, function(index, rawChunk) {
                console.log('chunk', index + 1);
                var splitChunk = rawChunk.split('\n');
                splitChunk[0] = currTail + splitChunk[0];
                currTail = splitChunk.pop();
                rawLines = rawLines.concat(splitChunk);
            });
            if (currTail != "") {
                rawLines.push(currTail);
            }

            db_head = rawLines[0].split(DELIMITER);

            keyToIndex = {};
            $.each(db_head, function(index, value) {
                keyToIndex[value] = index;
            });

            console.log(rawLines.length - 1);
            rawLines = rawLines.slice(1).map(parseLineOrNull).filter(function(e) { return e !== null; });

            console.log(rawLines.length, 'filter by IVS');

            $(document).trigger(Events.RAW_LINES_READY);
            return rawLines;
        }

        function parseFile(evt){
            var file = evt.target.files[0];
            if (!file) {
                console.log('no file found');
                return;
            }

            $(document).trigger(Events.FILE_READING_START);

            var chunkSize = 1024 * 1024 * 50;
            var fileSize = (file.size - 1);

            var chunks = [];
            var addedChunksCounter = 0;
            var readChunksCounter = 0;

            rawLines = [];

            var processChunk = function(e, index){
                chunks[index] = e.target.result;
                console.log('processing chunk', index);
                addedChunksCounter += 1;
                if (addedChunksCounter * chunkSize >= fileSize) {
                    console.log('last');
                    transformChunks(chunks);
                }
            };

            for(var i = 0; i < fileSize; i += chunkSize) {
                var reader = new FileReader();
                var blob = file.slice(i, chunkSize + i);
                reader.onload = (function(index) {
                    return function(e) { processChunk(e, index); }
                })(readChunksCounter);
                reader.readAsText(blob);
                readChunksCounter += 1;
            }
        }
    });
})(jQuery);